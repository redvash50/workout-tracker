import { reactive, watch } from 'vue';

// Models
import { Configuration } from '../models/configuration.model';
import { WorkoutWeek } from '../models/weeks.enum';

const CONFIGURATION_LOCAL_STORAGE_KEY = 'configuration';
const DEFAULT_CONFIGURATION: Configuration = {
  currentWeek: WorkoutWeek.Deload,
  bbPercentage: 50,
  minimumWeight: 1.25,
  minimumBarWeight: 20,
};

const loadConfiguration = (): Configuration => {
  const configurationData = localStorage.getItem(CONFIGURATION_LOCAL_STORAGE_KEY);

  if (configurationData) {
    // Merge default configuration and saved configuration in case new values were added
    // to default that aren't saved yet
    return {
      ...DEFAULT_CONFIGURATION,
      ...JSON.parse(configurationData)
    };
  }

  return DEFAULT_CONFIGURATION;
}
const saveConfiguration = (configuration: Configuration) => {
  localStorage.setItem(CONFIGURATION_LOCAL_STORAGE_KEY, JSON.stringify(configuration));
}

export const ConfigurationStoreState = reactive(loadConfiguration());

// Save changes to configuration into local storage
watch(ConfigurationStoreState, saveConfiguration);
