import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// @ts-ignore
import { version } from './package.json';

// https://vitejs.dev/config/
export default defineConfig({
  define: {
    'APP_VERSION': JSON.stringify(version)
  },
  plugins: [vue()],
});
