import { test, expect } from '@playwright/test';
import { getCurrentPage, goToStatsPage, RouteName } from "../helpers/navigation.helpers.js";

test.beforeEach(async ({ page }) => {
    await page.goto('http://localhost:5173');
});

test('Should show the "No exercises" warning in the workout view', async ({ page }) => {
    const noExercisesWarning = page.getByText('No exercises to show. Click below to go to settings and register your exercises');
    const goToSettingsButton = page
        .getByRole('main')
        .getByRole('link', { name: 'Settings' });

    await expect(noExercisesWarning).toBeVisible();
    await expect(goToSettingsButton).toBeVisible();
});

test('Should show the "No exercises" warning in the stats view', async ({ page }) => {
    await goToStatsPage(page);

    const noExercisesWarning = page.getByText('No exercises to show. Click below to go to settings and register your exercises');
    const goToSettingsButton = page
        .getByRole('main')
        .getByRole('link', { name: 'Settings' });

    await expect(noExercisesWarning).toBeVisible();
    await expect(goToSettingsButton).toBeVisible();
});

test('Should go to the settings view when clicking the button', async ({ page }) => {
    const goToSettingsButton = page
        .getByRole('main')
        .getByRole('link', { name: 'Settings' });

    await goToSettingsButton.click();

    // Wait for page transition to happen
    await page.waitForTimeout(1000);

    const currentPage = getCurrentPage(page);

    expect(currentPage).toBe(RouteName.Settings);
});
