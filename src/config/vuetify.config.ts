import { createVuetify } from 'vuetify';

import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

// Vuetify styles
import 'vuetify/styles';
import '@mdi/font/css/materialdesignicons.min.css'

export const vuetify = createVuetify({
  components,
  directives,
  theme: {
    themes: {
      light: {
        dark: true
      }
    }
  }
});
