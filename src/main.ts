import { createApp } from 'vue';

// Global Styles
import './style.scss';

// Config
import { router } from './config/router.config';
import { vuetify } from './config/vuetify.config';

// Components
import App from './App.vue';

window.addEventListener('appinstalled', () => {
    navigator.serviceWorker.controller?.postMessage({
        type: 'APP_INSTALLED'
    });
})

createApp(App)
  .use(vuetify)
  .use(router)
  .mount('#app')
