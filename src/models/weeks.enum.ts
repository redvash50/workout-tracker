export enum WorkoutWeek {
  ThreeTimesFive,
  ThreeTimesThree,
  FiveThreeOne,
  Deload
}

export function getWorkoutDescription(week: WorkoutWeek) {
  switch (week) {
    case WorkoutWeek.ThreeTimesFive: return '3x5';
    case WorkoutWeek.ThreeTimesThree: return '3x3';
    case WorkoutWeek.FiveThreeOne: return '5/3/1';
    case WorkoutWeek.Deload: return 'Deload';
  }
}
