import { Page } from "@playwright/test";

export const getImportButton = (page: Page) => {
    return page.getByRole('button', { name: 'Import' });
}

export const getExportButton = (page: Page) => {
    return page.getByRole('button', { name: 'Export' });
}

export const getAddButton = (page: Page) => {
    return page.getByRole('button', { name: 'Add' });
}

export const getBBPercentageInput = (page: Page) => {
    return page.getByRole('textbox', { name: 'BB percentage' })
}

export const getMinimumWeightInput = (page: Page) => {
    return page.getByRole('textbox', { name: 'Minimum weight' })
}

export const getMinimumBarWeightInput = (page: Page) => {
    return page.getByRole('textbox', { name: 'Minimum bar weight' })
}

