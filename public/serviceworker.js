// This code executes in its own worker or thread

const CACHE_VERSION = 4;
const CACHE_FILES = [
/*FILE_LIST*/
];
const CACHE_NAME = 'cache_' + CACHE_VERSION;

function isRequestCacheable(request) {
    return request.destination === 'document' ||
        CACHE_FILES.some(file => request.url.indexOf(file) !== -1);
}

function installApplicationFiles() {
    caches
        .open(CACHE_NAME)
        .then((cache) => {
            CACHE_FILES.forEach(file => {
                const request = new Request(file);

                // Check if file is already in cache, and if not, fetch it
                cache.match(request)
                    .then(cacheResponse => {
                        if(!cacheResponse) {
                            fetch(request).then(response => cache.put(request, response));
                        }
                    })
            });
        });
}

self.addEventListener("install", () => {
    console.log("Service worker installed");
});

self.addEventListener("activate", event => {
    console.log("Service worker activated");

    event.waitUntil(
        caches
            .keys()
            .then((cacheNames) => Promise.all(
                cacheNames.map((cacheName) => {
                    if (cacheName !== CACHE_NAME) {
                        return caches.delete(cacheName);
                    }
                })
            ))
    )
});

self.addEventListener('fetch', event => {
    console.log('Handling fetch event for', event.request.url);

    event.respondWith(
        caches.open(CACHE_NAME).then((cache) => {
            return cache.match(event.request)
                .then((cacheResponse) => {
                    if (cacheResponse) {
                        // If there is an entry in the cache for event.request, then response will be defined,
                        // and we can just return it. Note that in this example, only font resources are cached.
                        console.log(' Found response in cache:', cacheResponse);

                        return cacheResponse;
                    }

                    // Otherwise, if there is no entry in the cache for event.request, response will be
                    // undefined, and we need to fetch() the resource.
                    console.log(' No response for %s found in cache. About to fetch from network...', event.request.url);

                    // We call .clone() on the request since we might use it in a call to cache.put() later on.
                    // Both fetch() and cache.put() "consume" the request, so we need to make a copy.
                    // (see https://fetch.spec.whatwg.org/#dom-request-clone)
                    return fetch(event.request.clone())
                        .then((fetchResponse) => {
                            console.log('  Response for %s from network is: %O', event.request.url, fetchResponse);

                            if (fetchResponse.status < 400 && isRequestCacheable(event.request)) {
                                console.log('  Caching the response to', event.request.url);
                                cache.put(event.request, fetchResponse.clone());
                            } else {
                                console.log('  Not caching the response to', event.request.url);
                            }

                            // Return the original response object, which will be used to fulfill the resource request.
                            return fetchResponse;
                        });
            }).catch((error) => {
                // This catch() will handle exceptions that arise from the match() or fetch() operations.
                // Note that an HTTP error response (e.g. 404) will NOT trigger an exception.
                // It will return a normal response object that has the appropriate error code set.
                console.error('  Error in fetch handler:', error);

                throw error;
            });
        })
    );
});

self.addEventListener('message', event => {
    if (event.data.type === 'APP_INSTALLED') {
        console.log('Application installed. Installing data files.')
        installApplicationFiles();
    }
});


