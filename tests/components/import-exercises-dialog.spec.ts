import { test, expect } from '@playwright/test';
import path from 'path';
import { goToSettingsPage } from '../helpers/navigation.helpers.js';
import { getImportButton } from '../helpers/views/settings-view.helpers.js';
import mockData from '../mocks/mock-import-data.json' assert { type: 'json' };
import {
    getCancelButton,
    getConfirmButton,
    getFileInput,
    getTitle
} from '../helpers/components/import-exercises-dialog.helper.js';

test.beforeEach(async ({ page }) => {
    await page.goto('http://localhost:5173');
    await goToSettingsPage(page);
    await getImportButton(page).click();
});

test('Should show all the elements correctly', async ({ page }) => {
    const title = getTitle(page);
    const description = page.getByText('Select the file with the exercise data you wish to import:');
    const input = getFileInput(page);
    const confirmButton = getConfirmButton(page);
    const cancelButton = getCancelButton(page);

    await expect(title).toBeVisible();
    await expect(description).toBeVisible();
    await expect(input).toBeVisible();
    await expect(confirmButton).toBeVisible();
    await expect(cancelButton).toBeVisible();
});

test('Should close when clicking the cancel button', async ({ page }) => {
    await getCancelButton(page).click();

    await expect(getTitle(page)).not.toBeVisible();
});

test('Should close when clicking outside the modal', async ({ page }) => {
    await page.mouse.move(10, 10);
    await page.mouse.down();
    await page.mouse.up();

    // Wait for dialog to close
    await page.waitForTimeout(1000);

    await expect(getTitle(page)).not.toBeVisible();
});

test('Should correctly parse the imported data', async ({ page }) => {
    const input = getFileInput(page);

    await input.setInputFiles(path.resolve(process.cwd(), './tests/mocks/mock-import-data.json'));

    await Promise.all(mockData.map(async entry => {
        const entryName = page.getByText(entry.name);
        const entryInitialWeight = page.getByText(`Initial Weight: ${entry.initialWeight}`);
        const entryTracking = page.getByText(`Tracking: ${entry.tracking.length} entries`);

        await expect(entryName).toBeVisible();
        await expect(entryInitialWeight).toBeVisible();
        await expect(entryTracking).toBeVisible();
    }));
});

test('Should correctly add the imported data as exercises', async ({ page }) => {
    const input = getFileInput(page);
    const confirmButton = getConfirmButton(page);

    await input.setInputFiles(path.resolve(process.cwd(), './tests/mocks/mock-import-data.json'));

    await confirmButton.click();

    // Wait for dialog to close
    await page.waitForTimeout(1000);

    await Promise.all(mockData.map(async entry => {
        const entryName = page.getByText(entry.name);

        await expect(entryName).toBeVisible();
    }));
});
