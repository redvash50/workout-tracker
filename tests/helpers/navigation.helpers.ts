import { Page } from "@playwright/test";

export enum RouteName {
    Workout,
    Stats,
    Settings,
    ExerciseDetails
}

export const getCurrentPage = (page: Page) => {
    const currentUrl =  page.url();

    if (currentUrl.indexOf('workout') !== -1) {
        return RouteName.Workout;
    }
    if (currentUrl.indexOf('stats') !== -1) {
        return RouteName.Stats;
    }
    if (currentUrl.indexOf('settings') !== -1) {
        return RouteName.Settings;
    }
    if (currentUrl.indexOf('exercise') !== -1) {
        return RouteName.ExerciseDetails;
    }
}

export const goToWorkoutPage = (page: Page) => {
    return page
        .getByRole('banner')
        .getByRole('link', { name: 'Workout' })
        .click();
}

export const goToStatsPage = (page: Page) => {
    return page
        .getByRole('banner')
        .getByRole('link', { name: 'Stats' })
        .click();
}

export const goToSettingsPage = (page: Page) => {
    return page
        .getByRole('banner')
        .getByRole('link', { name: 'Settings' })
        .click();
}
