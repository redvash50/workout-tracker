import { createRouter, createWebHashHistory} from 'vue-router';

export enum RouteNames {
  ExerciseDetails = 'Exercise Details',
  Settings = 'Settings',
  Stats = 'Stats',
  Workout = 'Workout',
}

const routes = [
  {
    path: '/',
    redirect: '/workout'
  },
  {
    name: RouteNames.ExerciseDetails,
    path: '/exercise/:exerciseId?',
    component: () => import('../views/exercise-details.view.vue')
  },
  {
    name: RouteNames.Workout,
    path: '/workout',
    component: () => import('../views/workout.view.vue')
  },
  {
    name: RouteNames.Stats,
    path: '/stats',
    component: () => import('../views/stats.view.vue')
  },
  {
    name: RouteNames.Settings,
    path: '/settings',
    component: () => import('../views/settings.view.vue')
  }
];

export const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return {
        ...savedPosition,
        behavior: 'smooth',
      }
    } else {
      return {
        top: 0,
        behavior: 'smooth',
      }
    }
  },
});

