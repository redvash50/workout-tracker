import { v4 as generateUUID } from "uuid";

export class Exercise {
  id: string;
  name: string;
  initialWeight: number;
  scalePercentage: number;

  constructor() {
    this.id = generateUUID();
    this.name = '';
    this.initialWeight = Exercise.defaultValues.initialWeight;
    this.scalePercentage = Exercise.defaultValues.scalePercentage;
  }

  static get defaultValues() {
    return {
      initialWeight: 20,
      scalePercentage: 100,
    }
  }

  static parseExerciseData(data: Partial<Exercise>) {
    const newInstance = new Exercise();

    newInstance.id = data.id || newInstance.id;
    newInstance.name = data.name || newInstance.name;
    newInstance.initialWeight = data.initialWeight || newInstance.initialWeight;
    newInstance.scalePercentage = data.scalePercentage || newInstance.scalePercentage;

    return newInstance;
  }

  clone() {
    return Exercise.parseExerciseData(this);
  }
}
