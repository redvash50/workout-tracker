import { WorkoutWeek } from './weeks.enum';

export interface Tracking {
  exerciseId: string;
  weight: number;
  reps: number;
  date: string;
  week: WorkoutWeek
}
