const fs = require('fs');
const path = require('path');

function getDistFileList() {
    const distPath = path.join(__dirname, 'dist');

    const pathList = fs.readdirSync(distPath);
    const filesList = [];

    while (pathList.length > 0) {
        const nextFileName = pathList.shift()
        const nextPath = path.join(distPath, nextFileName);

        const isDirectory = fs.lstatSync(nextPath).isDirectory();

        if (isDirectory) {
            const innerPathList = fs.readdirSync(nextPath);

            innerPathList.forEach(path => pathList.push(`${nextFileName}/${path}`));
        } else {
            filesList.push(nextFileName);
        }
    }

    return filesList;
}

function setServiceWorkerFileList(fileList) {
    const serviceWorkerPath = path.join(__dirname, 'dist/serviceworker.js');
    const serviceWorkerData = fs.readFileSync(serviceWorkerPath)
        .toString()
        .split('\n');

    const lineToReplaceIndex = serviceWorkerData.findIndex(line => line.indexOf('FILE_LIST') !== -1);

    if (lineToReplaceIndex === -1) {
        throw 'File list placeholder not found';
    }

    serviceWorkerData.splice(lineToReplaceIndex, 1, ...fileList.map(file => `   '${file}',`));

    fs.writeFileSync(serviceWorkerPath, serviceWorkerData.join('\n'));
}

setServiceWorkerFileList(getDistFileList());
