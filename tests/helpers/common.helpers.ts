import { Page } from '@playwright/test';
import path from 'path';

import { getConfirmButton, getFileInput } from './components/import-exercises-dialog.helper.js';
import { goToSettingsPage } from './navigation.helpers.js';
import { getImportButton } from './views/settings-view.helpers.js';

export const loadMockData = async (page: Page) => {
    await goToSettingsPage(page);
    await getImportButton(page).click();
    await getFileInput(page)
        .setInputFiles(path.resolve(process.cwd(), './tests/mocks/mock-import-data.json'));
    await getConfirmButton(page).click();
}
