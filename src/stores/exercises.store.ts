import { reactive, watch } from 'vue';

// Models
import { Exercise } from '../models/exercise.model';

const EXERCISES_LOCAL_STORAGE_KEY = 'exercises';

const loadExercises = () => {
  const exercisesData = localStorage.getItem(EXERCISES_LOCAL_STORAGE_KEY);

  if (exercisesData) {
    const exercises = JSON.parse(exercisesData) as Exercise[];

    return exercises.map(exercise => Exercise.parseExerciseData(exercise));
  }

  return [];
}
const saveExercises = (exercises: Exercise[]) => {
  localStorage.setItem(EXERCISES_LOCAL_STORAGE_KEY, JSON.stringify(exercises));
}

export const ExercisesStoreState = reactive({
  exercises: loadExercises(),
});

export const ExercisesStoreSelectors = {
  getExerciseById: (exerciseId: string) => {
    return ExercisesStoreState.exercises.find(exercise => exercise.id === exerciseId);
  },
}

export const ExercisesStoreActions = {
  saveExercise: (exercise: Exercise) => {
    const selectedExerciseIndex = ExercisesStoreState.exercises.findIndex(existingExercise => existingExercise.id === exercise.id);

    if (selectedExerciseIndex !== -1) {
      ExercisesStoreState.exercises[selectedExerciseIndex] = exercise;
    } else {
      ExercisesStoreState.exercises.push(exercise);
    }
  },
  deleteExercise: (exerciseId: string) => {
    const selectedExerciseIndex = ExercisesStoreState.exercises.findIndex(existingExercise => existingExercise.id === exerciseId);

    if (selectedExerciseIndex !== -1) {
      ExercisesStoreState.exercises.splice(selectedExerciseIndex, 1);
    }
  },
}

// Save changes to exercises into local storage
watch(ExercisesStoreState.exercises, saveExercises);
