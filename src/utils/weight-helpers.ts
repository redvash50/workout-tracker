// Models
import { WorkoutWeek } from '../models/weeks.enum';

// Stores
import { ConfigurationStoreState } from "../stores/configuration.store";

export function getMaxRep(weight: number, reps: number) {
    return Math.round(36 * weight / (37 - reps));
  }
export function getRepsToMaxRep(wantedWeight: number, currentMaxRep: number) {
    return Math.round(37 - ((36 * wantedWeight) / currentMaxRep));
  }
export function getWeightPerWeekAndSet(maxWeight: number, scale: number, set: number, week: WorkoutWeek) {
    if (set === 1 && week === WorkoutWeek.ThreeTimesFive) {
      return normalizeWeight((maxWeight * 0.65) * (scale / 100));
    }
    if (set === 1 && week === WorkoutWeek.ThreeTimesThree) {
      return normalizeWeight((maxWeight * 0.70) * (scale / 100));
    }
    if (set === 1 && week === WorkoutWeek.FiveThreeOne) {
      return normalizeWeight((maxWeight * 0.75) * (scale / 100));
    }
    if (set === 1 && week === WorkoutWeek.Deload) {
      return normalizeWeight((maxWeight * 0.40) * (scale / 100));
    }
    if (set === 2 && week === WorkoutWeek.ThreeTimesFive) {
      return normalizeWeight((maxWeight * 0.75) * (scale / 100));
    }
    if (set === 2 && week === WorkoutWeek.ThreeTimesThree) {
      return normalizeWeight((maxWeight * 0.80) * (scale / 100));
    }
    if (set === 2 && week === WorkoutWeek.FiveThreeOne) {
      return normalizeWeight((maxWeight * 0.85) * (scale / 100));
    }
    if (set === 2 && week === WorkoutWeek.Deload) {
      return normalizeWeight((maxWeight * 0.50) * (scale / 100));
    }
    if (set === 3 && week === WorkoutWeek.ThreeTimesFive) {
      return normalizeWeight((maxWeight * 0.85) * (scale / 100));
    }
    if (set === 3 && week === WorkoutWeek.ThreeTimesThree) {
      return normalizeWeight((maxWeight * 0.90) * (scale / 100));
    }
    if (set === 3 && week === WorkoutWeek.FiveThreeOne) {
      return normalizeWeight((maxWeight * 0.95) * (scale / 100));
    }

    return normalizeWeight((maxWeight * (ConfigurationStoreState.bbPercentage / 100)) * (scale / 100));
  }
export function normalizeWeight(weight: number) {
    const minimumWeight = ConfigurationStoreState.minimumWeight * 2;
    const minimumBarWeight = ConfigurationStoreState.minimumBarWeight;

    // Calculated weight is below the bar's weight,
    // do the exercise with just the bar
    if (weight < minimumBarWeight) {
      weight = minimumBarWeight;
    }
    // Calculated weight cannot be set with the available weights,
    // round to the nearest available one
    if (weight % minimumWeight !== 0) {
      weight = minimumWeight * Math.ceil(weight / minimumWeight)
    }

    return weight;
  }

