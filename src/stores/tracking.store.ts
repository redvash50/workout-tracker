import { reactive, watch } from 'vue';
import moment from "moment/moment";

// Models
import { Tracking } from '../models/tracking.model';
import { WorkoutWeek } from '../models/weeks.enum';

interface TrackingStoreState {
  [id: string]: Tracking[];
}

const TRACKING_LOCAL_STORAGE_KEY = 'tracking';

const loadState = (): TrackingStoreState => {
  const stateData = localStorage.getItem(TRACKING_LOCAL_STORAGE_KEY);

  if (stateData) {
    return JSON.parse(stateData);
  }

  return {};
};
const saveState = (state: TrackingStoreState) => {
  localStorage.setItem(TRACKING_LOCAL_STORAGE_KEY, JSON.stringify(state));
};

export const TrackingStoreState = reactive({
  results: loadState()
});

export const TrackingStoreSelectors = {
  getExerciseTrackingById: (exerciseId: string): Tracking[] => {
    const exerciseTracking = TrackingStoreState.results[exerciseId];

    if (exerciseTracking) {
      return exerciseTracking.sort((firstEntry, secondEntry) => {
        const firstEntryDate = moment(firstEntry.date).valueOf();
        const secondEntryDate = moment(secondEntry.date).valueOf();

        return firstEntryDate < secondEntryDate ? 1 : -1;
      });
    }

    return [];
  },
  getLastTrackingByExerciseId: (exerciseId: string, week?: WorkoutWeek): Tracking | null => {
    const selectedExerciseTracking = TrackingStoreSelectors.getExerciseTrackingById(exerciseId);

    if (selectedExerciseTracking) {
      if (week) {
        return selectedExerciseTracking.find((tracking) => tracking.week === week) || null;
      }
      return selectedExerciseTracking[0];
    }

    return null;
  },
}

export const TrackingStoreActions = {
  deleteExerciseTracking: (exerciseId: string) => {
    delete TrackingStoreState.results[exerciseId];
  },
  deleteExerciseTrackingEntry: (exerciseId: string, entryIndex: number) => {
    TrackingStoreState.results[exerciseId].splice(entryIndex, 1);
  },
  registerResults: (tracking: Tracking) => {
    if (TrackingStoreState.results[tracking.exerciseId]) {
      TrackingStoreState.results[tracking.exerciseId].push(tracking);
    }
    else {
      TrackingStoreState.results[tracking.exerciseId] = [tracking];
    }
  },
}

watch(TrackingStoreState.results, saveState);
