import { Page } from "@playwright/test";

export const getTitle = (page: Page) => {
    return page.getByText('Import exercises');
}
export const getFileInput = (page: Page) => {
    return page.getByRole('textbox', { name: 'File input' });
}
export const getCancelButton = (page: Page) => {
    return page.getByRole('button', { name: 'Cancel' });
}
export const getConfirmButton = (page: Page) => {
    return page.getByRole('button', { name: 'Confirm' });
}
