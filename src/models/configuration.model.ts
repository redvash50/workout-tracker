import { WorkoutWeek } from './weeks.enum';

export interface Configuration {
  currentWeek: WorkoutWeek;
  bbPercentage: number;
  minimumWeight: number;
  minimumBarWeight: number
}
